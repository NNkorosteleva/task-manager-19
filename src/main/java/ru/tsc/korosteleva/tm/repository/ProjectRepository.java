package ru.tsc.korosteleva.tm.repository;

import ru.tsc.korosteleva.tm.api.repository.IProjectRepository;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.model.Project;

import java.util.Date;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String name) {
        final Project project = new Project();
        project.setName(name);
        return add(project);
    }

    @Override
    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public Project create(final String name,
                          final String description,
                          final Date dateBegin,
                          final Date dateEnd) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return add(project);
    }

    @Override
    public Project findOneByName(final String name) {
        for (Project project : records) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project updateById(final String id,
                              final String name,
                              final String description) {
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index,
                                 final String name,
                                 final String description) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeByName(final String name) {
        final Project project = findOneByName(name);
        if (project == null) return null;
        records.remove(project);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) {
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

}