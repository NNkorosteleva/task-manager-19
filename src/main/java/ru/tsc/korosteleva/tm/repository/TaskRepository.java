package ru.tsc.korosteleva.tm.repository;

import ru.tsc.korosteleva.tm.api.repository.ITaskRepository;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.model.Task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String name) {
        final Task task = new Task();
        task.setName(name);
        return add(task);
    }

    @Override
    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public Task create(final String name,
                       final String description,
                       final Date dateBegin,
                       final Date dateEnd) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return add(task);
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : records) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) result.add(task);
        }
        return result;
    }

    @Override
    public Task findOneByName(final String name) {
        for (Task task : records) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task updateById(final String id,
                           final String name,
                           final String description) {
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index,
                              final String name,
                              final String description) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        records.remove(task);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

}