package ru.tsc.korosteleva.tm.service;

import ru.tsc.korosteleva.tm.api.repository.IProjectRepository;
import ru.tsc.korosteleva.tm.api.service.IProjectService;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.korosteleva.tm.exception.field.*;
import ru.tsc.korosteleva.tm.model.Project;

import java.util.Date;

public class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(name);
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(name, description);
    }

    @Override
    public Project create(final String name,
                          final String description,
                          final Date dateBegin,
                          final Date dateEnd) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (dateBegin == null) throw new DateIncorrectException();
        if (dateEnd == null) throw new DateIncorrectException();
        return repository.create(name, description, dateBegin, dateEnd);
    }

    @Override
    public Project findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Project project = repository.findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project updateById(final String id,
                              final String name,
                              final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Project project = repository.updateById(id, name, description);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index,
                                 final String name,
                                 final String description) {
        if (index == null || index < 0 || index > repository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Project project = repository.updateByIndex(index, name, description);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project removeByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Project project = repository.removeByName(name);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Project project = repository.changeProjectStatusById(id, status);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index > repository.getSize()) throw new IndexIncorrectException();
        Project project = repository.changeProjectStatusByIndex(index, status);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

}