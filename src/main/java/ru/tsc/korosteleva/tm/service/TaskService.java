package ru.tsc.korosteleva.tm.service;

import ru.tsc.korosteleva.tm.api.repository.ITaskRepository;
import ru.tsc.korosteleva.tm.api.service.ITaskService;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.exception.entity.TaskNotFoundException;
import ru.tsc.korosteleva.tm.exception.field.*;
import ru.tsc.korosteleva.tm.model.Task;

import java.util.Collections;
import java.util.Date;
import java.util.List;

public class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(name);
    }

    @Override
    public Task create(final String name,
                       final String description,
                       final Date dateBegin,
                       final Date dateEnd) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (dateBegin == null) throw new DateIncorrectException();
        if (dateEnd == null) throw new DateIncorrectException();
        return repository.create(name, description, dateBegin, dateEnd);
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(name, description);
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(projectId);
    }

    @Override
    public Task findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Task task = repository.findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task updateById(final String id,
                           final String name,
                           final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        Task task = repository.updateById(id, name, description);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index,
                              final String name,
                              final String description) {
        if (index == null || index < 0 || index > repository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        Task task = repository.updateByIndex(index, name, description);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task removeByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Task task = repository.removeByName(name);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Task task = repository.changeTaskStatusById(id, status);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index > repository.getSize()) throw new IndexIncorrectException();
        Task task = repository.changeTaskStatusByIndex(index, status);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

}