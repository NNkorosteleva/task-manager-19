package ru.tsc.korosteleva.tm.service;

import ru.tsc.korosteleva.tm.api.repository.IUserRepository;
import ru.tsc.korosteleva.tm.api.service.IUserService;
import ru.tsc.korosteleva.tm.enumerated.Role;
import ru.tsc.korosteleva.tm.exception.entity.UserNotFoundException;
import ru.tsc.korosteleva.tm.exception.user.*;
import ru.tsc.korosteleva.tm.model.User;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    public UserService(final IUserRepository repository) {
        super(repository);
    }

    @Override
    public User create(final String login,
                       final String password,
                       final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExist(login)) throw new LoginExistException(login);
        if (isEmailExist(email)) throw new EmailExistException(email);
        return repository.create(login, password, email);
    }

    @Override
    public User create(final String login,
                       final String password,
                       final String email,
                       final String role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExist(login)) throw new LoginExistException(login);
        if (isEmailExist(email)) throw new EmailExistException(email);
        if (role == null || role.isEmpty()) throw new RoleEmptyException();
        if (isRoleIncorrect(role)) throw new RoleIncorrectException(role);
        return repository.create(login, password, email, role);
    }

    @Override
    public User updateUser(final String id,
                           final String firstName,
                           final String lastName,
                           final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = repository.updateUser(id, firstName, lastName, middleName);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = repository.setPassword(id, password);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public User findOneByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findOneByLogin(login);
    }

    @Override
    public User findOneByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findOneByEmail(email);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = repository.removeByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    private boolean isEmailExist(final String email) {
        return findOneByEmail(email) != null;
    }

    private boolean isLoginExist(final String login) {
        return findOneByLogin(login) != null;
    }

    private boolean isRoleIncorrect(String role) {
        return Role.toRole(role) == null;
    }

}
