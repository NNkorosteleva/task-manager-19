package ru.tsc.korosteleva.tm.api.repository;

import ru.tsc.korosteleva.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(AbstractCommand command);

    Collection<AbstractCommand> getTerminalCommands();

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArgument(String argument);
}
