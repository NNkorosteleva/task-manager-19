package ru.tsc.korosteleva.tm.api.service;

import ru.tsc.korosteleva.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    void add(AbstractCommand command);

    Collection<AbstractCommand> getTerminalCommands();

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArgument(String argument);
}
