package ru.tsc.korosteleva.tm.api.model;

import ru.tsc.korosteleva.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
