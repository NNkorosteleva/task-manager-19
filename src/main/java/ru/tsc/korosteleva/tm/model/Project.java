package ru.tsc.korosteleva.tm.model;

import ru.tsc.korosteleva.tm.api.model.IWBS;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.util.DateUtil;

import java.util.Date;

public final class Project extends AbstractModel implements IWBS {

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date created = new Date();

    private Date dateBegin;

    private Date dateEnd;

    public Project() {
    }

    public Project(final String name,
                   final String description,
                   final Status status,
                   final Date dateBegin) {
        this.name = name;
        this.description = description;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(final Date created) {
        this.created = created;
    }

    @Override
    public void setDateCreated(final Date created) {
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(final Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(final Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    @Override
    public String toString() {
        return name + " : "
                + description + " : "
                + getStatus().getDisplayName() + " : "
                + DateUtil.toString(getCreated()) + " : "
                + DateUtil.toString(getDateBegin());
    }

}