package ru.tsc.korosteleva.tm.command.user;

import ru.tsc.korosteleva.tm.model.User;
import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class UserFindByLoginCommand extends AbstractUserCommand {

    public static final String NAME = "user-find-by-login";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Find user by login.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[FIND USER BY LOGIN]");
        System.out.println("[ENTER LOGIN:]");
        String login = TerminalUtil.nextLine();
        final User user = getUserService().findOneByLogin(login);
        showUser(user);
    }
}
