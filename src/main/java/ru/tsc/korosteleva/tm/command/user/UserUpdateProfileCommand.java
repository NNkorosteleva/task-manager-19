package ru.tsc.korosteleva.tm.command.user;

import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    public static final String NAME = "user-update";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Update user.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER]");
        String userId = getAuthService().getUserId();
        System.out.println("[ENTER FIRST NAME:]");
        String firstName = TerminalUtil.nextLine();
        System.out.println("[ENTER LAST NAME:]");
        String lastName = TerminalUtil.nextLine();
        System.out.println("[ENTER MIDDLE NAME:]");
        String middleName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }
}
