package ru.tsc.korosteleva.tm.command.user;

import ru.tsc.korosteleva.tm.model.User;

public class UserViewProfileCommand extends AbstractUserCommand {

    public static final String NAME = "user-view-profile";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "View user profile.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[VIEW USER PROFILE]");
        final User user = getAuthService().getUser();
        showUser(user);
    }
}
