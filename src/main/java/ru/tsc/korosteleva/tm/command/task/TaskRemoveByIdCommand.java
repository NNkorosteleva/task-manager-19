package ru.tsc.korosteleva.tm.command.task;

import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class TaskRemoveByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-remove-by-id";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Remove task by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getTaskService().removeById(id);
    }

}
