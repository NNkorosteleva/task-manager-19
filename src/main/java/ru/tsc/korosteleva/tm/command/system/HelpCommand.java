package ru.tsc.korosteleva.tm.command.system;

import ru.tsc.korosteleva.tm.command.AbstractCommand;

import java.util.Collection;

public class HelpCommand extends AbstractSystemCommand {

    public static final String NAME = "help";

    public static final String ARGUMENT = "-h";

    public static final String DESCRIPTION = "Show terminal commands.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) System.out.println(command);
    }

}
