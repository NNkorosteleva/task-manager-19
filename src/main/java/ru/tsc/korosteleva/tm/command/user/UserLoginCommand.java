package ru.tsc.korosteleva.tm.command.user;

import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractUserCommand {

    public static final String NAME = "user-login";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Log in.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("[ENTER LOGIN:]");
        String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }
}
