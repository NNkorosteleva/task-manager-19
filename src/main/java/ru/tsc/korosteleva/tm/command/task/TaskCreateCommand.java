package ru.tsc.korosteleva.tm.command.task;

import ru.tsc.korosteleva.tm.util.TerminalUtil;

import java.util.Date;

public class TaskCreateCommand extends AbstractTaskCommand {

    public static final String NAME = "task-create";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Create new task.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        System.out.println("ENTER DATA BEGIN (DD.MM.YYYY):");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("ENTER DATA END (DD.MM.YYYY):");
        final Date dateEnd = TerminalUtil.nextDate();
        getTaskService().create(name, description, dateBegin, dateEnd);
    }

}
