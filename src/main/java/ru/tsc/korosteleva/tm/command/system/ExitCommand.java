package ru.tsc.korosteleva.tm.command.system;

public class ExitCommand extends AbstractSystemCommand {

    public static final String NAME = "exit";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Close application.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
