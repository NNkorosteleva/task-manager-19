package ru.tsc.korosteleva.tm.command.user;

import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractUserCommand {

    public static final String NAME = "user-remove-by-login";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Remove user by login.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("[ENTER LOGIN:]");
        String login = TerminalUtil.nextLine();
        getUserService().removeByLogin(login);
    }
}
