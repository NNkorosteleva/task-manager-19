package ru.tsc.korosteleva.tm.command.task;

import ru.tsc.korosteleva.tm.model.Task;
import ru.tsc.korosteleva.tm.util.TerminalUtil;

import java.util.List;

public class TaskListByProjectCommand extends AbstractTaskCommand {

    public static final String NAME = "task-list-by-project-id";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Show project by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = getTaskService().findAllByProjectId(projectId);
        renderTasks(tasks);
    }

}
