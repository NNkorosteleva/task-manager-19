package ru.tsc.korosteleva.tm.command.user;

import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class UserRemoveByIdCommand extends AbstractUserCommand {

    public static final String NAME = "user-remove-by-id";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Remove user by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER BY ID]");
        System.out.println("[ENTER ID:]");
        String id = TerminalUtil.nextLine();
        getUserService().removeById(id);
    }
}
