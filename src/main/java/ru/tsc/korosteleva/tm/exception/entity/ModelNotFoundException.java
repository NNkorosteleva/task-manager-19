package ru.tsc.korosteleva.tm.exception.entity;

import ru.tsc.korosteleva.tm.exception.AbstractException;

public class ModelNotFoundException extends AbstractException {

    public ModelNotFoundException() {
        super("Error! Model not found.");
    }

}
